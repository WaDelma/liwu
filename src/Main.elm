module Main exposing (..)

import Browser
import Browser.Events as E
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as HtmlAttr
import Html.Events exposing (onInput, onClick, custom)
import Html.Events.Extra.Mouse as Mouse
import Css
import Svg.Styled as Svg exposing (Svg)
import Svg.Styled.Attributes as SvgAttr
import Svg.Events exposing (onMouseOver)
import Keyboard.Event as KE
import Json.Decode as D
import Draggable
import Draggable.Events as DE
import Array exposing (Array)
import Tuple

-- Currently used palette: https://coolors.co/545454-69747c-6baa75-84dd63-cbff4d
-- + rgb(62, 148, 195)

theme =
  { highlight = "rgb(62, 148, 195)"
  , background = "rgb(74, 74, 74)"
  , backgroundAlternate = "rgb(94, 94, 89)"
  , backgroundHighlight = "rgb(84, 84, 84)"
  , grid = "rgb(105, 116, 124)"
  , gridHighlight = "rgb(107, 170, 117)"
  , note = "rgb(132, 221, 99)"
  , noteEdge = "rgb(203, 255, 77)"
  }

-- MAIN

main : Program () Model Msg
main = Browser.element
  { init = init
  , update = update
  , view = view >> Html.toUnstyled
  , subscriptions = subscriptions
  }

condMap : (a -> a) -> Bool -> a -> a
condMap f c a = if c then f a else a

maybeAppend : Maybe a -> List a -> List a
maybeAppend a list = case a of
  Just aa -> list ++ [aa]
  Nothing -> list

type alias Vec2 a = (a, a)

componentwise : (a -> a -> b) -> Vec2 a -> Vec2 a -> Vec2 b
componentwise f (a1, aa1) (a2, aa2) = (f a1 a2, f aa1 aa2)

add : Vec2 Float -> Vec2 Float -> Vec2 Float
add = componentwise (+)

sub : Vec2 Float -> Vec2 Float -> Vec2 Float
sub = componentwise (-)

minVec : Vec2 Float -> Vec2 Float -> Vec2 Float
minVec = componentwise min

maxVec : Vec2 Float -> Vec2 Float -> Vec2 Float
maxVec = componentwise max

type alias Id = String

type alias Note =
  { id: Id
  , lane: Int
  , from: Float
  , to: Float
  }

length : Note -> Float
length { from, to } = to - from

-- TODO: Prevent moving note outside the area
dragNoteBy : Sizes -> Float -> MetricStructure -> Int -> Vec2 Float -> Bool -> MovingNote -> Note
dragNoteBy sizes barLength metricStructure subdivision (pdx, pdy) smoothDrag ({ note } as movingNote) =
  let
    -- TODO: Remove hard coded numbers
    y = movingNote.originalLane + truncate (pdy / sizes.laneHeight)
    x = condMap
      (xOfPart metricStructure subdivision barLength >> ((+) movingNote.fromOffset))
      (not smoothDrag)
      (movingNote.originalFrom + pdx)
  in
    { note
    | from = x
    , to = x + length note
    , lane = y
    }

stretchNoteBy : Sizes -> Float -> MetricStructure -> Int -> Vec2 Float -> Bool -> MovingNote -> Note
stretchNoteBy sizes barLength metricStructure subdivision (pdx, pdy) smoothStrech ({ note } as movingNote) =
  let
    x = condMap
      (xOfPart metricStructure subdivision barLength >> ((+) movingNote.toOffset))
      (not smoothStrech)
      (movingNote.originalTo + pdx)
  in { note | to = clamp (note.from + 1) (4 * barLength) x }

type alias MovingNote =
  { note: Note
  , side: Side
  , fromOffset: Float
  , toOffset: Float
  , originalLane: Int
  , originalFrom: Float
  , originalTo: Float
  , delta: Vec2 Float
  }

startMoving : Side -> Float -> Float -> Note -> MovingNote
startMoving side fromOffset toOffset note = MovingNote
  note
  side
  fromOffset
  toOffset
  note.lane
  note.from
  note.to
  (0, 0)


type alias NoteGroup =
  { currentId: Int
  , movingNotes: List MovingNote
  , placedNotes: List Note
  , lanes: Int
  }

emptyGroup : NoteGroup
emptyGroup = NoteGroup 0 [] [] 20

findNote : NoteGroup -> Id -> Maybe Note
findNote group id =
  allNotes group
    |> List.filter (.id >> (==) id)
    |> List.head

addNote : Int -> Float -> Float -> NoteGroup -> (Id, NoteGroup)
addNote lane from to ({ currentId, placedNotes } as group) =
  let id = String.fromInt currentId
  in
    ( id
    , { group
      | placedNotes =
          Note id lane from to :: placedNotes
      , currentId = currentId + 1
      }
    )

removeNotes : List Id -> NoteGroup -> NoteGroup
removeNotes ids group =
  { group
    | placedNotes = allNotes group
      |> List.filter (.id >> (\id -> List.member id ids) >> not)
    , movingNotes = []
  }

allNotes : NoteGroup -> List Note
allNotes { movingNotes, placedNotes } =
  (movingNotes |> List.map .note) ++ placedNotes

-- allNotesAnnotated : NoteGroup -> List (Bool, Note)
-- allNotesAnnotated { movingNotes, placedNotes } =
--   let
--     moving = movingNotes |> List.map (.note >> (Tuple.pair True))
--     placed = placedNotes |> List.map (Tuple.pair False)
--   in moving ++ placed

startDragging : MetricStructure -> Int -> Float -> List Id -> Side -> NoteGroup -> NoteGroup
startDragging metricStructure subdivision barLength ids side group =
    let
      (targetAsList, others) = allNotes group
        |> List.partition (.id >> (\id -> List.member id ids))
    in
      { group
      | placedNotes = others
      , movingNotes = targetAsList
        |> List.map (\note -> 
          let
            from = note.from
            to = note.to
            gridFrom = xOfPart metricStructure subdivision barLength from
            gridTo = xOfPart metricStructure subdivision barLength to
          in
            startMoving side (from - gridFrom) (to - gridTo) note
        )
      }

stopDragging : NoteGroup -> NoteGroup
stopDragging group =
  { group
  | placedNotes = allNotes group
  , movingNotes = []
  }

mapMovingNotes: (MovingNote -> Vec2 Float -> Note) -> Vec2 Float -> NoteGroup -> NoteGroup
mapMovingNotes f delta group =
  { group
  | movingNotes = group.movingNotes
    |> List.map (\note ->
      let newDelta = add note.delta delta
      in
        { note
        | note = f note newDelta
        , delta = newDelta
        }
    )
  }

type alias DragMeta =
  { pos: Vec2 Float
  , offset: Vec2 Float
  , key: Int
  , altKey: Bool
  , ctrlKey: Bool
  }

type alias MetricStructure =
  { upper: Array Int
  , lower: Int
  }

xOfPart : MetricStructure -> Int -> Float -> Float -> Float
xOfPart metricStructure subdivision barLength x =
  let
    -- Bars
    (_, startOfBarX, inBarX) = splitToParts barLength x
    -- Splits
    splits = Array.length metricStructure.upper
    splitWidth = barLength / toFloat splits
    (whichSplit, startOfSplitX, inSplitX) = splitToParts splitWidth inBarX
    -- Parts
    parts =  Array.get whichSplit metricStructure.upper
      |> Maybe.withDefault 0
    partWidth = splitWidth / toFloat parts
    (whichPart, startOfPartX, inPartX) = splitToParts partWidth inSplitX
    -- Subdivisions
    subdivisionWidth = partWidth / (toFloat subdivision)
    (_, startOfSubdivision, _) = splitToParts subdivisionWidth inPartX
  in startOfBarX + startOfSplitX + startOfPartX + startOfSubdivision

splitToParts : Float -> Float -> (Int, Float, Float)
splitToParts width value =
  let which = floor (value / width)
      start = toFloat which * width
  in (which, start, value - start)

type alias Sizes =
  { laneHeight: Float
  , noteHeight: Float
  }

type alias Model =
  { pianoRollPos: Vec2 Float
  , notes: NoteGroup
  , noteLength: Float
  , barLength: Float
  , subdivision: Int
  , drag: Draggable.State ()
  , target: Maybe (DragTarget, DragMeta)
  , focusedLane: Maybe Int
  , metricStructure: MetricStructure
  , sizes: Sizes
  }

init : () -> (Model, Cmd Msg)
init _ =
  let laneHeight = 10
  in
    ( Model
        (10, 10)
        emptyGroup
        20
        200
        4
        Draggable.init
        Nothing
        Nothing
        { upper = Array.fromList [4, 3], lower = 4 }
        (Sizes laneHeight laneHeight)
    , Cmd.none
    )

getTarget : Model -> Vec2 Float
getTarget { target } =
  target
    |> Maybe.map Tuple.second
    |> Maybe.map .pos
    |> Maybe.withDefault (0, 0)

getOffset : Model -> Vec2 Float
getOffset { target } =
  target
    |> Maybe.map Tuple.second
    |> Maybe.map .offset
    |> Maybe.withDefault (0, 0)

dragConfig : Draggable.Config () Msg
dragConfig = Draggable.customConfig
  [ DE.onDragBy OnDragBy
  , DE.onDragStart StartDragging
  , DE.onDragEnd StopDragging
  , DE.onMouseDown OnMouseDown
  , DE.onClick OnClick
  ]

-- UPDATE

type Side = Middle | Right

type DragTarget
  = NotesTgt NotesTarget
  | SelectBoxTgt SelectBoxTarget

type alias NotesTarget = (List Id, Side)
type alias SelectBoxTarget = (Vec2 Float, Vec2 Float)

notesTarget : DragTarget -> Maybe NotesTarget
notesTarget target = case target of
  NotesTgt t -> Just t
  _ -> Nothing

selectBoxTarget : DragTarget -> Maybe SelectBoxTarget
selectBoxTarget target = case target of
  SelectBoxTgt t -> Just t
  _ -> Nothing

type Msg
  = PlaceNote Int (Draggable.Msg ()) DragMeta
  | OnClick () Int
  | DragMsg (Draggable.Msg ()) (DragTarget, DragMeta)
  | OnDragBy Draggable.Delta
  | StartDragging ()
  | StopDragging
  | OnMouseDown () Int
  | FocusOn Int
  | Defocus
  | OnKeyDown KE.KeyboardEvent
  | NoOp

update : Msg -> Model -> (Model, Cmd Msg)
update msg ({ notes, noteLength, metricStructure, sizes, pianoRollPos, barLength, subdivision } as model) =
  let
    _ = ()
    _ = Debug.log "update" msg
    -- _ = Debug.log "target" model.target
  in case msg of
    PlaceNote lane dragMsg ({ key, altKey, ctrlKey } as dragMeta) -> case key of
      0 ->
        (
          if ctrlKey then
            { model
            | target = Just 
              ( SelectBoxTgt (dragMeta.pos, dragMeta.pos)
              , { dragMeta
                | offset = (0, 0)
                }
              )
            }
          else
            let
              curPos = sub dragMeta.pos (Tuple.first pianoRollPos, 0)
              x = condMap
                (xOfPart metricStructure subdivision barLength)
                (not altKey)
                (Tuple.first curPos)
              (id, newNotes) = notes |> addNote lane x (x + noteLength)
            in
              { model
              | notes = newNotes
              , target = Just 
                ( NotesTgt ([id], Middle)
                , { dragMeta
                  | offset = (0, 0)
                  , pos = curPos
                  }
                )
              }
        ) |> Draggable.update dragConfig dragMsg
      _ -> ( model, Cmd.none )
    OnDragBy delta ->
      case model.target of
        Just ((NotesTgt _), meta) ->
          let
            smoothDrag = meta.altKey
          in
            ( { model
              | notes = notes
                |> (mapMovingNotes
                  (\note newDelta ->
                    case note.side of
                      Middle -> dragNoteBy
                        sizes
                        barLength
                        metricStructure
                        subdivision
                        newDelta
                        smoothDrag
                        note
                      Right -> stretchNoteBy
                        sizes
                        barLength
                        metricStructure
                        subdivision
                        newDelta
                        smoothDrag
                        note
                  )
                  delta
                )
              }
            , Cmd.none
            )
        Just ((SelectBoxTgt (from, to)), meta) ->
          ( { model
            | target = Just 
              ( SelectBoxTgt (from, add to delta)
              , meta
              )
            }
          , Cmd.none
          )
        Nothing -> (model, Cmd.none)
    StartDragging _ ->
      ( model.target
        |> Maybe.andThen (\(t, m) -> notesTarget t |> Maybe.map (\tt -> (tt, m)))
        |> Maybe.map (\((infos, side), meta) ->
            { model
            | notes = notes
              |> startDragging metricStructure subdivision barLength infos side
            }
          )
        |> Maybe.withDefault model
      , Cmd.none
      )
    StopDragging ->
      ( { model
        | notes = notes
          |> stopDragging
        , noteLength = notes
          |> .movingNotes
          |> List.head
          |> Maybe.map .note
          |> Maybe.map length
          |> Maybe.withDefault noteLength
        , target =
            model.target
              |> Maybe.andThen (\(t, meta) -> case t of
                  SelectBoxTgt (from, to) ->
                    let
                      (fx, fy) = sub (minVec from to) pianoRollPos
                      (tx, ty) = sub (maxVec from to) pianoRollPos
                      selectedNotes = allNotes model.notes
                        |> List.filter (\note ->
                          not (note.to < fx
                              || note.from > tx
                              || toFloat (note.lane + 1) * sizes.noteHeight < fy
                              || toFloat note.lane * sizes.noteHeight > ty
                          )
                        )
                    in if (List.isEmpty selectedNotes) then
                      Nothing
                    else
                      Just
                        ( NotesTgt 
                          ( selectedNotes |> List.map .id
                          , Middle
                          )
                        -- TODO: This is old metadata
                        , meta
                        )
                  NotesTgt _ -> if meta.ctrlKey then Just (t, meta) else Nothing
                )
        }
      , Cmd.none
      )
    DragMsg dragMsg ((tgt, meta) as target) ->
    -- TODO: This needs to be only once, but cannot distinguish events here. Ditch elm-draggable?
      { model
      | target = (if meta.ctrlKey then
            case (model.target |> Maybe.map Tuple.first, tgt) of
              (Just (NotesTgt (ids, _)), NotesTgt (newIds, side)) ->
                let
                  -- TODO: This event is triggered twice so the code in comments doesn't work. Replace library with own thing?
                  -- This tries to allow removing selecting by ctrl clicking already selected notes
                  new = newIds --|> List.filter (\id -> not (List.member id ids))
                  old = ids |> List.filter (\id -> not (List.member id newIds))
                in
                  (NotesTgt
                    ( new ++ old
                    , side
                    )
                  , meta
                  )
              _ -> target
          else
            target
        ) |> Just
      }
        |> Draggable.update dragConfig dragMsg
    OnMouseDown _ button -> case button of
      1 ->
        ( model.target
          |> Maybe.map Tuple.first
          |> Maybe.andThen notesTarget
          -- TODO: Only the node under cursor should be removed
          |> Maybe.andThen (\(f, s) -> f |> List.head |> Maybe.map (\ff -> (ff, s)))
          |> Maybe.map
            (\info ->
              { model
              | notes = notes
                |> removeNotes [Tuple.first info]
              })
          |> Maybe.withDefault model
        , Cmd.none
        )
      _ ->
        ( { model
          | noteLength = model.target
            |> Maybe.map Tuple.first
            |> Maybe.andThen notesTarget
            |> Maybe.map Tuple.first
            -- TODO: What to select for `noteLength` when there are multiple notes selected?
            |> Maybe.andThen List.head
            |> Maybe.andThen (findNote model.notes)
            |> Maybe.map length
            |> Maybe.withDefault noteLength
          }
        , Cmd.none
        )
    OnClick _ button -> case button of
      0 ->
        ( { model
          | target = case model.target of
            Just (NotesTgt (ids, side), meta) ->
              if meta.ctrlKey then
                model.target
              else
                List.tail ids
                  |> Maybe.map (\idss -> (NotesTgt (idss, side), meta))
                  |> Maybe.withDefault (NotesTgt (ids, side), meta)
                  |> Just
            _ -> model.target
          }
        , Cmd.none
        )
      _ -> (model, Cmd.none)
    OnKeyDown event ->
      case event.key |> Maybe.map String.toLower of
        Just "delete" ->
          case model.target of
            Just ((NotesTgt (ids, _)), _) ->
              ( { model
                | target = Nothing
                , notes = notes |> removeNotes ids
                }
              , Cmd.none
              )
            _ -> (model, Cmd.none)
        Just "a" -> if event.ctrlKey then
              ( { model
                | target = Just
                    ( NotesTgt (allNotes model.notes |> List.map .id, Middle)
                    , { altKey = event.altKey
                      , ctrlKey = event.ctrlKey
                      , key = 0
                      , offset = (0, 0)
                      , pos = (0, 0)
                      }
                    )
                }
              , Cmd.none
              )
            else
              (model, Cmd.none)
        _ -> (model, Cmd.none)
    FocusOn lane -> ({ model | focusedLane = Just lane }, Cmd.none)
    Defocus -> ({ model | focusedLane = Nothing }, Cmd.none)
    NoOp -> (model, Cmd.none)

-- VIEW

view : Model -> Html Msg
view ({ notes, pianoRollPos, metricStructure, sizes, barLength, subdivision, focusedLane, target } as model) =
  Svg.svg
    [ SvgAttr.viewBox "0 0 1920 1080"
    , SvgAttr.width "1920"
    , SvgAttr.height "1080"
    ]
    [ Svg.node "g" []
      (
        [ gridBackgroundView barLength pianoRollPos subdivision metricStructure sizes
        , lanesView barLength notes.lanes pianoRollPos sizes focusedLane
        , gridView barLength pianoRollPos subdivision metricStructure sizes
        , notesView notes pianoRollPos sizes (
          target |> Maybe.andThen (\(tgt, _) -> notesTarget tgt)
            |> Maybe.map Tuple.first
            |> Maybe.withDefault []
        )
        ] |> maybeAppend (
            target
              |> Maybe.map Tuple.first
              |> Maybe.andThen selectBoxTarget
              |> Maybe.map selectBoxView
          )
      )
    ]

selectBoxView : (Vec2 Float, Vec2 Float) -> Svg Msg
selectBoxView (from, to) =
  let
    (fx, fy) = minVec from to
    (tx, ty) = maxVec from to
  in
  Svg.rect
    [ SvgAttr.x (String.fromFloat fx)
    , SvgAttr.y (String.fromFloat fy)
    , SvgAttr.width (String.fromFloat (tx - fx))
    , SvgAttr.height (String.fromFloat (ty - fy))
    , SvgAttr.fill "none"
    , SvgAttr.stroke theme.highlight
    , SvgAttr.strokeWidth "0.75"
    -- , SvgAttr.strokeDasharray "3,3"
    ]
    []

-- TODO: Make bars alternate background color
lanesView : Float -> Int -> Vec2 Float -> Sizes -> Maybe Int -> Svg Msg
lanesView barLength lanes (pianoRollX, pianoRollY) sizes focusedLane =
  List.range 0 (lanes - 1)
    |> (List.map (\lane ->
      let
        x = String.fromFloat pianoRollX
        y = String.fromFloat (pianoRollY + sizes.laneHeight * toFloat lane)
        isFocused = Maybe.map ((==) lane) focusedLane
          |> Maybe.withDefault False
      in
        Svg.rect
          [ SvgAttr.x x
          , SvgAttr.y y
          , SvgAttr.width (String.fromFloat (barLength * 4))
          , SvgAttr.height (String.fromFloat sizes.laneHeight)
          , SvgAttr.fill (if isFocused then theme.backgroundHighlight else "transparent")
          , SvgAttr.stroke theme.grid
          , SvgAttr.strokeWidth "0.5"
          , onMouseOver (FocusOn lane) |> SvgAttr.fromUnstyled
          , Draggable.customMouseTrigger dragMetadataDecoder (PlaceNote lane)
            |> SvgAttr.fromUnstyled
          ]
          []
      ))
    |> Svg.node "g" []

notesView : NoteGroup -> Vec2 Float -> Sizes -> List Id -> Svg Msg
notesView group pianoRollPos sizes ids =
  group
    |> allNotes
    |> List.reverse
    |> List.map (noteView sizes pianoRollPos ids)
    |> Svg.node "g" []

noteView : Sizes -> Vec2 Float -> List Id -> Note -> Svg Msg
noteView sizes (pianoRollX, pianoRollY) ids ({ id, lane, from, to } as note) =
  let
    active = List.member id ids
    atX x = String.fromFloat (x + pianoRollX)
    y = String.fromFloat (sizes.laneHeight * toFloat lane + pianoRollY + 0.5)
  in Svg.node "g" []
    [ Svg.rect
      [ SvgAttr.x (atX from)
      , SvgAttr.y y
      , SvgAttr.width (String.fromFloat (length note))
      , SvgAttr.height (String.fromFloat (sizes.noteHeight - 1))
      , SvgAttr.cursor "move"
      , SvgAttr.fill (if active then theme.highlight else theme.note)
      , SvgAttr.fillOpacity "0.5"
      , SvgAttr.stroke (if active then theme.highlight else theme.noteEdge)
      , SvgAttr.strokeWidth "0.25"
      , onMouseOver (FocusOn lane) |> SvgAttr.fromUnstyled
      , Draggable.customMouseTrigger (dragInfoDecoder id Middle) DragMsg
        |> SvgAttr.fromUnstyled
      ]
      []
    , Svg.rect
      [ SvgAttr.x (atX (to - 1))
      , SvgAttr.y y
      , SvgAttr.width "1.5"
      , SvgAttr.height (String.fromFloat (sizes.noteHeight - 1))
      , SvgAttr.cursor "col-resize"
      , SvgAttr.fill (if active then theme.highlight else theme.noteEdge)
      , SvgAttr.pointerEvents "all"
      -- , SvgAttr.visibility "hidden"
      , onMouseOver (FocusOn lane) |> SvgAttr.fromUnstyled
      , Draggable.customMouseTrigger (dragInfoDecoder id Right) DragMsg
        |> SvgAttr.fromUnstyled
      ]
      []
    ]

gridBackgroundView : Float -> Vec2 Float -> Int -> MetricStructure -> Sizes -> Svg Msg
gridBackgroundView barLength pianoRollPos subdivision structure sizes =
  List.range 0 3
    |> List.map (\n ->
        let
          (x, y) = add pianoRollPos (barLength * toFloat n, 0)
        in Svg.rect
          [ SvgAttr.x (String.fromFloat x)
          , SvgAttr.y (String.fromFloat y)
          , SvgAttr.width (String.fromFloat barLength)
          , SvgAttr.height (String.fromFloat (y + sizes.laneHeight * 19))
          , SvgAttr.fill (
              if modBy 2 n == 0 then
                theme.background
              else
                theme.backgroundAlternate
            )
          ]
          []
      )
    |> Svg.node "g" []

gridView : Float -> Vec2 Float -> Int -> MetricStructure -> Sizes -> Svg Msg
gridView barLength pianoRollPos subdivision structure sizes =
  List.range 0 3
    |> List.map (\n ->
        add pianoRollPos (barLength * toFloat n, 0)
          |> barView sizes subdivision structure barLength
      )
    |> Svg.node "g" []

barView : Sizes -> Int -> MetricStructure -> Float -> (Float, Float) -> Svg Msg
barView sizes subdivision structure width (x, y) =
  let
    splits = Array.length structure.upper
    widthPer = width / toFloat splits
  in
    Array.foldl
      (\parts (n, elems) ->
        let toNextDiv = widthPer / toFloat parts
        in
          ( n + widthPer
          , elems ++
            List.concatMap
              (\m ->
                let toNext = toNextDiv / toFloat subdivision
                in
                  List.range 1 subdivision
                    |> (List.map (\i ->
                      let xx = String.fromFloat (x + n + toNextDiv * (toFloat m - 1) +  toNext * (toFloat i - 1) )
                      in Svg.line
                        [ SvgAttr.x1 xx
                        , SvgAttr.y1 (String.fromFloat y)
                        , SvgAttr.x2 xx
                        , SvgAttr.y2 (String.fromFloat (y + sizes.laneHeight * 20))
                        , SvgAttr.strokeWidth (if n == 0 && m == 1 && i == 1 then "1" else "0.5")
                        , SvgAttr.stroke (if i == 1 then theme.gridHighlight else theme.grid)
                        , SvgAttr.pointerEvents "none"
                        ]
                        []))
              )
              (List.range 1 parts)
          )
      )
      (0, [])
      structure.upper
    |> Tuple.second
    |> Svg.node "g" []

dragInfoDecoder : Id -> Side -> D.Decoder (DragTarget, DragMeta)
dragInfoDecoder id side =
  D.map2 Tuple.pair
    (D.succeed (NotesTgt ([id], side)))
    dragMetadataDecoder

dragMetadataDecoder : D.Decoder DragMeta
dragMetadataDecoder =
  D.map5 DragMeta
    positionDecoder
    offsetDecoder
    buttonDecoder
    -- TODO: Double clicking alt should snap to grid
    -- TODO: This only notices if alt was pressed before clicking
    -- It should be possible to turn on and off smooth drag while dragging
    -- To fix this keyboard key events should be tracked.
    -- This could be done with `elm/browser`, `Gizra/elm-keyboard-event` or with `ohanhi/keyboard`
    altKeyDecoder
    ctrlKeyDecoder

positionDecoder : D.Decoder (Vec2 Float)
positionDecoder =
  D.map2 Tuple.pair
    (D.field "pageX" D.float)
    (D.field "pageY" D.float)

offsetDecoder : D.Decoder (Vec2 Float)
offsetDecoder =
  D.map2 Tuple.pair
    (D.field "offsetX" D.float)
    (D.field "offsetY" D.float)

altKeyDecoder : D.Decoder Bool
altKeyDecoder = D.field "altKey" D.bool

ctrlKeyDecoder : D.Decoder Bool
ctrlKeyDecoder = D.field "ctrlKey" D.bool

buttonDecoder : D.Decoder Int
buttonDecoder = D.field "button" D.int

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions { drag, target } =
  Sub.batch
    [ case target of
        Just tgt -> Draggable.subscriptions (\msg -> DragMsg msg tgt) drag
        Nothing -> Sub.none
    , E.onKeyDown (D.map OnKeyDown KE.decodeKeyboardEvent)
    ]