module Draggable2 exposing (..)

type alias Msg = ()

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none