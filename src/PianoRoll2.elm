module PianoRoll2 exposing (..)
import Array exposing (Array)
import Maybe exposing (andThen)

addNote: PianoRoll -> NoteLocation -> Maybe PianoRoll
addNote {grid, index} {start, end} =
  let
    (start_x, start_y) = start
    (end_x, end_y) = end
  in
    get start_x start_y grid |> andThen (\from_note ->
    get end_x end_y grid |> andThen (\to_note ->
      case (from_note, to_note) of
        (Nothing, Nothing) -> Just
          { grid = (
              set start_x start_y (Just
                { ty = NoteStart
                , id = index
                }
              )
              >>
              set end_x end_y (Just
                { ty = NoteEnd
                , id = index
                }
              )
            ) grid
          , index = index + 1
          }
        _ -> Nothing
    ))

get: Int -> Int -> Grid -> Maybe (Maybe Note)
get x y grid =
    Array.get y grid |> andThen (\lane -> Array.get x lane)

set: Int -> Int -> Maybe Note -> Grid -> Grid
set x y node grid =
  case (Array.get y grid) |> Maybe.map (\lane ->
    Array.set
      y
      (Array.set x node lane)
      grid
  ) of
    Nothing -> grid
    Just g -> g

type alias NoteLocation =
  { start: Coord
  , end: Coord
  }

type alias Coord = (Int, Int)

type alias PianoRoll =
  { grid : Grid
  , index : Int
  }

type alias Grid = Array Lane
type alias Lane = Array (Maybe Note)

type alias NoteId = Int

type alias Note =
  { id: NoteId
  , ty: NoteType
  }
type NoteType 
  = NoteStart
  | NoteEnd
  | NoteBoth
